<?php

$getPost = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$setPost = array_map('strip_tags', $getPost);
$post = array_map('trim', $setPost);


$Action = $post['action'];
$json = array();
unset($post['action']);
sleep(1);

if ($Action):
    require '../_app/Config.inc.php';
    $Read = new Read;
    $Create = new Create;
    $update = new Update();
    $delete = new Delete();
endif;
switch ($Action) {
    case 'create':
        if (in_array('', $post)):
            $json['error'] = '<b>Opsss:</b> Para cadastrar um usuário preencha todos os campos!';
        elseif (!Check::Email($post['user_email'])):
            $json['error'] = '<b>Opss:</b> Para cadastrar um usuário, o Email precisa ser válido';
        elseif (strlen($post['user_password']) < 5 OR strlen($post['user_password']) > 10):
            $json['error'] = 'Sua senha deve ter de 5 a 10 caracteres';
        else:
            $Read->FullRead("SELECT user_id FROM ws_users WHERE user_email = :email", "email={$post['user_email']}");
            if ($Read->GetResult()):
                $json['error'] = " <b>Oppss</b> O email <b>{$post['user_email']}</b> já está cadastrado";
            else:
                $Create->ExeCreate('ws_users', $post);
                if ($Create->GetResult()):
                    $json['success'] = 'Cadastro realizado com Sucesso';
                    $json['result'] = "<article style='display:none' class='user_box j_register' id='{$Create->GetResult()}'><h1>{$post['user_name']} {$post['user_lastname']}</h1><p>{$post['user_email']} (Nível {$post['user_level']})</p><a class='action edit j_edit' rel='{$Create->getResult()}'>Editar</a><a class='action del' rel='{$Create->GetResult()}'>Deletar</a></article>";
                endif;
            endif;
        endif;
        break;
    case 'loadmore':
        $json['result'] = null;
        $Read->ExeRead('ws_users', "ORDER BY user_id DESC LIMIT :limit OFFSET :offset", "limit=2&offset={$post['offset']}");
        if ($Read->getResult()):
            foreach ($Read->getResult() as $User):
                extract($User);
                $json['result'] .= "<article style='display:none' class='user_box' id='{$user_id}'><h1>{$user_name} {$user_lastname}</h1><p>{$user_email} (Nível {$user_level})</p><a class='action edit j_edit' rel='{$user_id}'>Editar</a><a class='action del j_delete' rel='{$user_id}'>Deletar</a><img class='form_load load_read_{$user_id}' src='img/load.gif' alt='[CARREGANDO...]' title='CARREGANDO...'/></article>";
            endforeach;
        else:
            $json['result'] = "<div style='margin: 15px 0 0'class='trigger trigger-error'>Não Existem mais resultados</div>";
        endif;
        break;
    default:
        $json['error'] = 'Erro ao selecionar uma ação';
        break;

    case 'readuser':
         $Read->ExeRead('ws_users', "WHERE user_id = :id", "id={$post['user_id']}");
         if($Read->GetResult()):
             $json['user'] = $Read->GetResult()[0];
         else:
             $json['error'] = true;
         endif;
        break;
    case 'update':
        if (in_array('', $post)):
            $json['error'] = '<b>Opsss:</b> Para cadastrar um usuário preencha todos os campos!';
        elseif (!Check::Email($post['user_email'])):
            $json['error'] = '<b>Opss:</b> Para cadastrar um usuário, o Email precisa ser válido';
        elseif (strlen($post['user_password']) < 5 OR strlen($post['user_password']) > 10):
            $json['error'] = 'Sua senha deve ter de 5 a 10 caracteres';
        else:
            $Read->FullRead("SELECT user_id FROM ws_users WHERE user_email = :email AND user_id != :id", "email={$post['user_email']}&id={$post['user_id']}");
            if ($Read->GetResult()):
                $json['error'] = " <b>Oppss</b> O email <b>{$post['user_email']}</b> já está cadastrado";
            else:
                $user_ID = $post['user_id'];
                unset($post['user_id']);
                $update->ExeUpdate('ws_users', $post, "WHERE user_id = :id", "id={$user_ID}");
                if ($update->GetResult()):
                    $json['success'] = 'Usuário Atualizado com Sucesso';
                    $json['action'] = 'update';
                    $json['user_id'] = $user_ID;
                    $json['resultContent'] = "<article class='user_box' id='{$user_ID}'><h1>{$post['user_name']} {$post['user_lastname']}</h1><p>{$post['user_email']} (Nível {$post['user_level']})</p><a class='action edit j_edit' rel='{$user_ID}'>Editar</a><a class='action del' rel='{$user_ID}'>Deletar</a></article>";
                    $json['returnData'] = $post;
                   
                endif;
            endif;
        endif;
        break;
        
    case 'deleteuser':
        $delete->ExeDelete('ws_users', "WHERE user_id = :id", "id={$post['user_id']}");
        if($delete->GetResult()):
            $json['delete'] = true;
        endif;
        break;
}

echo json_encode($json);
