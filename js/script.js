$(function () {
    //Alert
    //alert('Hello World!');


    //SELETOR, EVENTO/EFEITO, CALLBACK, AÇÃO
    // $('.j_edit').click(function(event) {
    // 	//console.clear();
    // 	console.log('eu cliquei');
    // 	$('form').fadeOut(1000, function(){
    // 		alert('Terminou');
    // 	});

    // 	return false;
    // });
    // 
    // 
    // Shorthand´s
    // $('.j_formsubmit').submit(function(){
    // 	var dados = $(this).serialize();
    // 	$.post('ajax/ajax.php', dados, function(data) {
    // 		console.log(data);
    // 	});
    // 	return false;
    // });//Fim do submit

    //Fazendo form com atualização dinâmica, isso é demais!
    //    setInterval(function(){
    //        var dados = $('.j_formsubmit').serialize();
    //        alert(dados);
    //        $('.j_formsubmit').find('input[name=id]').val('21');
    //    },4000);
    //FIM DE FAZER FORM DINAMICO

    $('.j_formsubmit').submit(function () {
        var data = $(this).serialize();
        var dataDois = $('.j_formsubmit').serialize(); //Pode ser feito desta forma também
        var form = $(this);

        $.ajax({
            url: 'ajax/ajax.php',
            data: data,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                //aqui vem algo antes de enviar
                form.find('.form_load').fadeIn();
                //Aqui ao invés de pegar direto a trigger eu posso passar um form.find 
                //e pegar a trigger especifica do form
                $('.trigger').fadeOut(500, function () {
                    $(this).remove();
                });
            },
            success: function (resposta) {
                //aqui vem algo quando obter sucesso
                //console.log(resposta);

                if (resposta.error) {
                    $('.trigger-box').html('<div class="trigger trigger-error">' + resposta.error + '</div>');
                    $('.trigger-error').fadeIn();
                } else {
                    $('.trigger-box').html('<div class="trigger trigger-success">' + resposta.success + '</div>');
                    $('.trigger-success').fadeIn();

                    form.find('input[class!="noclear"]').val('');

                    $(resposta.result).prependTo($('.register').find('.j_list')); //Vai aparecer acima de tudo na lista que estiver dentro
                    //da classe result. 

                    $('.j_register').fadeIn(400);
                }
                form.find('.form_load').fadeOut();
                
                //Codigo usado para atualizar a chamada do front.
                if(resposta.action == 'update'){
                    $('#'+resposta.user_id).replaceWith(function(){
                       // window.location.reload(); PODEMOS USAR PARA RELOAD
                        return resposta.resultContent;
                    });
                }
            }
        });

        return false;
    });

    //Função para exibir o form
    $('.j_open').click(function () {
        $('.' + $(this).attr('rel')).slideDown();
        $('.j_userid').remove();
        $('.j_formsubmit').find('input[class!="noclear"]').val('');
        $('.j_formsubmit').find('input[name="action"]').val('create');
        $('.j_formsubmit').find('button').val('Cadastrar Usuário');
    });

    //Função para sumir com o form
    $('.j_close').click(function () {
        $('.' + $(this).attr('rel')).slideUp();
    });

    //Função para recarregar mais usuários
    $('.j_load').click(function () {
        var destino = $('.' + $(this).attr('rel'));
        var loaded = destino.find('article').length;
        $.ajax({
            url: 'ajax/ajax.php',
            data: {action: "loadmore", offset: loaded},
            type: 'POST',
            dataType: "json",
            beforeSend: function () {
                //alguma coisa antes de enviar
                destino.find('article').fadeTo(300, 0.5);
                //$('.jlist').fadeTo(300, 0.5);
                $('.more_result').fadeIn();
                $('.trigger-error').remove();
            },
            success: function (data) {
                //alguma coisa quando enviado
                console.log(data);
                //destino.html(data.result); //Errado pois este deleta os outros e coloca os que vierem do ajax
                //$('.j_list').html(data.result);
                //destino.find('.j_insert').html(data.result); Reseta tudo
                //$(data.result).appendTo(destino.find('.j_insert'));
                //$(data.result).prependTo(destino.find('.j_insert')); //Nesse caso sempre o ultimo resultado aparece
                $(data.result).appendTo(destino.find('.j_insert')); //AO contrário do acima ele irá colocar sempre por último
                $('.trigger, article').fadeIn('fast', function () {
                    $('.more_result').fadeOut();
                    $('.user_box').fadeTo(300, 1);
                });

            }

        });
    });

    //Função para a edição
    //Essa função pega apenas os dados que foram carregados junto com o DOM
//    $('.j_edit').click(function(){
//        var user_id = $(this).attr('rel');
//        alert(user_id);
//    });

    //Função para edição, essa aqui pode ser carregada sempre que o DOM for 
    //requisitado para trazer novos dados com ajax veja mais em "atualizando-via-ajax".
    //Sempre que recarregar com jQuery para manipular aquele elemento voce tem que seguir a lógica 
    //abaixo de monitorar  o Document Object Model
    $('.j_list').on('click', '.j_edit', function () {
        var user_id = $(this).attr('rel');

        $.ajax({
            url: 'ajax/ajax.php',
            type: 'post',
            dataType: 'json',
            data: {action: 'readuser', user_id: user_id},
            beforeSend: function () {
                //Aqui antes de enviar
                //$('.j_userid').remove();
                //$('.load_read_' + user_id).fadeIn();//Chama a classe e concatena com o ID dela.
                $('#'+user_id).find('.form_load').fadeIn();
            },
            success: function (resposta) {
                if (!$('.j_formsubmit').is(':visible') && !resposta.error) {
                     $('.j_open').click();
                }
                    if (resposta.error) {
                        alert('Usuário não encontrado no sistema');
                    } else {
                      $.each(resposta.user, function(key, value){
                          $('.j_formsubmit').find('input[name="'+key+'"]').val(value);
                      });
                      $('.button_edit').val('Editar Usuário');
                      $('.j_formsubmit').find('input[name="action"]').val('update');
                      $('<input type="hidden" class="j_userid" name="user_id" value="'+resposta.user.user_id+'"> ').prependTo('.j_formsubmit');
                    }
                //$('.load_read_' + user_id).fadeOut();//Chama a classe e concatena com o ID dela.
                $('#'+user_id).find('.form_load').fadeOut();
                

            }
        });
    });
    
    //Função de delete
    $('.j_list').on('click', '.j_delete', function(){
        var user_id = $(this).attr('rel');
        
        $.ajax({
            url: 'ajax/ajax.php',
            data: {action: 'deleteuser', user_id:user_id},
            dataType: 'json',
            type: 'POST',
            beforeSend: function(){
                $('.load_read_'+user_id).fadeIn();
            },
            success: function( resposta ){
                if(resposta.delete){
                    $('#'+user_id).fadeOut(400, function(){
                        $('#'+user_id).remove();
                    });
                    
                }else{
                    $('.load_read_'+user_id).fadeOut();
                    alert('erro ao deletar. Tente novamente');
                }
            }
        });
    });
});
